﻿using UnityEngine;

public sealed class World : MonoBehaviour
{
   static readonly  World _instance = new World();

   private static GameObject[] _hidingSpots;

   static World()
   {
      _hidingSpots = GameObject.FindGameObjectsWithTag("Hide");
   }
   
   private World(){}

   public static World Instance
   {
      get { return _instance; }
   }

   public GameObject[] GetHidingSpots()
   {
      return _hidingSpots;
   }
}
