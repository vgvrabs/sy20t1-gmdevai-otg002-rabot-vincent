﻿using UnityEngine;


public class EvadeAI : AIControl
{
   public override void Behavior()
   {
      if (Vector3.Distance(this.transform.position, target.transform.position) <= aggroRange)
      {
         Evade();
      }
      else
      {
         Wander();
      }
   }

   void Update()
   {
      Behavior();
   }
}
