﻿using UnityEngine;

public class HideAI : AIControl
{
    public override void Behavior()
    {
        if (Vector3.Distance(this.transform.position, target.transform.position) <= aggroRange)
        {
            CleverHide();
        }
        else
        {
            Wander();
        }
    }

    void Update()
    {
        Behavior();
    }
}
