﻿using UnityEngine;

public class PursueAI : AIControl
{
    public override void Behavior()
    {
        if (Vector3.Distance(this.transform.position, target.transform.position) <= aggroRange)
        {
            Pursue();
        }
        else
        {
            Wander();
        }
    }

    void Update()
    {
        Behavior();
    }
}
