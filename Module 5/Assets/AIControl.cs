﻿using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    NavMeshAgent _agent;

    public GameObject target;

    public WASDMovement playerMovement;

    public float aggroRange;



    void Start()
    {
        _agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();
        
    }

    protected  void Seek(Vector3 location)
    {
        _agent.SetDestination(location);
    }

    protected void Flee(Vector3 location) 
    {
        Vector3 fleeDirection = location - this.transform.position;
        _agent.SetDestination(this.transform.position - fleeDirection);
    }

    protected void Pursue()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude / (_agent.speed + playerMovement.currentSpeed);
        
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    protected void Evade()
    {
        Vector3 targetDir = target.transform.position - this.transform.position;

        float lookAhead = targetDir.magnitude / (_agent.speed + playerMovement.currentSpeed);

        Flee(target.transform.position + target.transform.forward * lookAhead);
    }

    private Vector3 _wanderTarget;

    protected void Wander()
    {
        float _wanderRadius = 20f;
        float _wanderDistance = 5f;
        float _wanderJitter = 1f;

        _wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * _wanderJitter,
            0, Random.Range(-1.0f, 1.0f) * _wanderJitter);
        _wanderTarget.Normalize();
        _wanderTarget *= _wanderRadius;

        Vector3 _targetLocal = _wanderTarget + new Vector3(0, 0, _wanderDistance);
        Vector3 _targetWorld = this.gameObject.transform.InverseTransformVector(_targetLocal);

        Seek(_targetWorld);
    }

    protected  void Hide()
    {
        float _distance = Mathf.Infinity;
        Vector3 _chosenSpot = Vector3.zero;

        int _hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < _hidingSpotsCount; i++)
        {
            Vector3 _hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 _hidePosition =  World.Instance.GetHidingSpots()[i].transform.position + _hideDirection.normalized * 5;

            float _spotDistance = Vector3.Distance(this.transform.position, _hidePosition);

            if (_spotDistance < _distance)
            {
                _chosenSpot = _hidePosition;
                _distance = _spotDistance;
            }
        }
        
        Seek(_chosenSpot);
    }
    
    protected void CleverHide()
    {
        float _distance = Mathf.Infinity;
        Vector3 _chosenSpot = Vector3.zero;
        Vector3 _chosenDirection = Vector3.zero;
        GameObject _chosenGameObject = World.Instance.GetHidingSpots()[0];

        int _hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < _hidingSpotsCount; i++)
        {
            Vector3 _hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 _hidePosition =  World.Instance.GetHidingSpots()[i].transform.position 
                                     + _hideDirection.normalized * 2;

            float _spotDistance = Vector3.Distance(this.transform.position, _hidePosition);

            if (_spotDistance < _distance)
            {
                _chosenSpot = _hidePosition;
                _chosenDirection = _hideDirection;
                _chosenGameObject = World.Instance.GetHidingSpots()[i];
                _distance = _spotDistance;
            }
        }

        Collider _hideCol = _chosenGameObject.GetComponent<Collider>();
        Ray _back = new Ray(_chosenSpot, -_chosenDirection.normalized);
        RaycastHit info;
        float _rayDistance = 100.0f;
        _hideCol.Raycast(_back, out info, _rayDistance);
        Seek(info.point + _chosenDirection.normalized * 5);
    }

    bool CanSeeTarget()
    {
        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
        if (Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo))
        {
            return raycastInfo.transform.gameObject.CompareTag("Player");
        }
        else
        {
            return false;
        }
    }

    public virtual void Behavior()
    {
        
    }
    

    void Update()
    {
        Wander();
    }
    
    
}
