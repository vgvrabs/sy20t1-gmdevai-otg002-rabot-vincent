﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetMovement : MonoBehaviour {
    
    public Transform Player;
 
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate() {
        
        
        if(Vector3.Distance(transform.position, Player.position) > 3f){
            transform.position = Vector3.Lerp(transform.position,Player.position, Time.deltaTime);
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, Player.rotation, Time.deltaTime * 100f);
        
    }
}
