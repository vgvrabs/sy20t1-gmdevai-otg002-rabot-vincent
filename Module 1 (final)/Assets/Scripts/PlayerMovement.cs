﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float rotSpeed;

    // Start is called before the first frame update
    void Start(){
        speed = 5.0f;
        rotSpeed = 0.5f;

    }

    void Movement(){

        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(horizontal, 0, vertical) * speed * Time.deltaTime;
        transform.position += movement;
    }

    // Update is called once per frame
    void LateUpdate() {

        Movement();
    }
}
