﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

    Vector3 mousePos;

      void Update() {
          Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
          RaycastHit hit;
          if(Physics.Raycast(ray, out hit)){
              mousePos = hit.point;

          }

          mousePos = new Vector3(mousePos.x, transform.position.y,mousePos.z);
          transform.LookAt(mousePos);
         
    }
}
