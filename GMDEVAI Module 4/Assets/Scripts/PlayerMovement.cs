﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Camera camera;
    public CharacterController controller;
    public float speed = 12f;
    void FixedUpdate() {

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        transform.rotation = Quaternion.LookRotation(camera.transform.forward, camera.transform.up);
        
        controller.Move(move * speed * Time.deltaTime);
    }
}