﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

    public Transform playerBody;

    float xRotation = 0f;
    float yRotation = 0f;

    public float mouseSens = 100f;
    void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    void Update() {
        float mouseX = Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime;

        yRotation += mouseX;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        //yRotation = Mathf.Clamp(yRotation, -90f, 90f);

        transform.eulerAngles = new Vector3(xRotation, yRotation, 0f);
    }
}
