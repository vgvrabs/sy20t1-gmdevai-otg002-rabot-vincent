﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.AI;

public class MouseInput : MonoBehaviour
{

    public GameObject obstacle;
    public GameObject waypoint;

    public GameObject[] agents;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                GameObject monster = Instantiate(obstacle, hit.point, obstacle.transform.rotation);
                foreach (GameObject a in agents)
                {
                   a.GetComponent<AIControl>().DetectNewObstacle(hit.point);
                }
                Destroy(monster, 10f);
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                GameObject newPath = Instantiate(waypoint, hit.point, waypoint.transform.rotation);
                foreach (GameObject a in agents)
                {
                    a.GetComponent<AIControl>().Seek(newPath.transform.position);
                }
                
                Destroy(newPath, 10f);
            }
        }
        
    }
}
