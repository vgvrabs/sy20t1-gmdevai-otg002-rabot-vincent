﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float maxHp;
    public float currentHp;
    private bool isAlive = true;
    void Start()
    {
        currentHp = maxHp;
    }

    public void TakeDamage()
    {
        currentHp -= 20;
        CheckIfAlive();
    }

    public bool CheckIfAlive()
    {
        if (currentHp <= 0)
        {
            this.gameObject.SetActive(false);
            isAlive = false;
        }
        return isAlive;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Projectile"))
        {
            this.TakeDamage();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
